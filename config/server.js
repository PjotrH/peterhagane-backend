module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  //url: env('NGINX_URL', ''),
  url: env('api.peterhagane.dev',''),
  cron:{
    enabled: true
  },
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET'),
    },
  },
});
